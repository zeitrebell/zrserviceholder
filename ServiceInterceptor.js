import fetchIntercept from 'fetch-intercept';
const tokenKey = 'jwtToken';


export const unregister = fetchIntercept.register({

    request: function (url, config) {

        let token = localStorage.getItem(tokenKey);
        if (token != undefined) {
            token = token.replace('"', '').replace('"', '');

            config['headers'] = {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json",
            };

        } else {
            window.location.href = "http://localhost:1337/admin";
        }
        return [url, config];
    },

    requestError: function (error) {
        return Promise.reject(error);
    },

    response: function (response) {
        if (response.code === 401) {
            window.location.href = "http://localhost:1337/admin";
        }
        // Modify the reponse object
        return response;
    },

    responseError: function (error) {
        // Handle an fetch error
        return Promise.reject(error);
    }
});