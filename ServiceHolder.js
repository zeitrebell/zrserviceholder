import { unregister } from './ServiceInterceptor'

export default class ServiceHolder {
    static myInstance = null;
    apiTables = [];


    static getInstance() {
        if (ServiceHolder.myInstance == null) {
            ServiceHolder.myInstance = new ServiceHolder();
        }
        return this.myInstance;
    }

    /**
     *  Initializer Method for Service Holder
     *
     *  param:  {apiUrl} the API uri for the endpoint
     *          {tableNames} array of string, the names of the existing tables
     *  return  
     */
    init(apiUrl) {
        this.apiUrl = apiUrl;
    }

    /**
    *  Gets a service for a specific table, or creates a new one and returns it
    *
    *  param:  {tableName} tablename string
    *  return  The Service for the specific table
    */
    getApiTable(tableName) {
        this.apiTables.map(item => {
            if (item === undefined) { return [] }
            if (item.key.localeCompare(tableName) == 0) {
                return item.value;
            }
        });
        this.apiTables.push(
            {
                key: tableName,
                value: new Service(this.apiUrl + tableName + "/")
            }
        );
        return this.apiTables[this.apiTables.length - 1].value;

    }


    /**
     *  Checks if a specific Service instance for a table exists
     *
     *  param:  {tableName} tablename string
     *  return  {boolean} if the tablename is existing as a Service
     */
    apiTableExists(tableName) {
        this.apiTables.forEach((item) => {
            if (item.key === tableName) {
                return true;
            }
        });
        return false;
    }

}



class Service {
    apiUrl = '';
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    /**
    *  Updates an iten into the backend
    *
    *  param: {item} The items to update
    *  return {Promise} Promise for the resolved Ajax Call result in JSON format
    */
    putItem(item, itemId) {
        let promise = new Promise((resolve) => {
            try {
                fetch(this.apiUrl + itemId, {
                    method: 'PUT',
                    body: JSON.stringify(item)
                }).then(response => resolve(response))
            }
            catch (err) {
                console.log(err);
                resolve(false);
            }
        });
        return promise;
    }

    /**
 *  Updates items into the backend
 *
 *  param: {items} The itemlist to update
 *  return {Promise} Promise for the resolved Ajax Call result in JSON format
 */
    putItems(items) {
        let promise = new Promise((resolve) => {
            let fResult = [];
            (items.forEach((item) => {
                this.putItem(item).then(
                    result => {
                        fResult.push(result);
                    }
                );
            }));
            resolve(fResult);
        });
        return promise;
    }


    /**
     *  Persists an iten into the backend
     *
     *  param: {item} The items to persist
     *  return {Promise} Promise for the resolved Ajax Call result in JSON format
     */
    postItem(item) {
        let promise = new Promise((resolve) => {
            try {
                fetch(this.apiUrl, {
                    method: 'POST',
                    body: JSON.stringify(item)
                }).then(response => { return response['body'].getReader(); })
                    .then(result => result.read()
                        .then(res => console.log(res)))
            }
            catch (err) {
                console.log(err);
                resolve(false);
            }
        });
        return promise;
    }

    /**
     *  Persists an itemlist into the backend
     *
     *  param: {items} the list of the items to persist
     *  return {Promise} Promise for the resolved Ajax Call result in JSON format
     */
    postItems(items) {
        let promise = new Promise((resolve) => {
            let fResult = [];
            (items.forEach((item) => {
                this.putItem(item).then(
                    result => {
                        fResult.push(result);
                    }
                );
            }));
            resolve(fResult);
        });
        return promise;
    }

    /**
     *  Gets all entries of the table
     *
     *  param:
     *  return {Promise} Promise for the resolved Ajax Call result in JSON format
     */
    getAllItems() {
        let promise = new Promise((resolve) => {
            try {
                fetch(this.apiUrl, { method: 'GET' })
                    .then(res => res.json())
                    .then(
                        result => {
                            resolve(result);
                        }
                    );
            } catch (err) {
                console.log(err);
            }
        });
        return promise;
    }


    /**
     *  Gets a specific entry of the table.
     *
     *  param: {id} The id string of the specific Entry
     *  return {Promise} Promise of the resolved Ajax Call result in JSON format
     */
    getItemById(itemId) {
        return this.specificItemById(itemId, 'GET');
    }

    /**
     *  Deletes a specific entry of the table.
     *
     *  param: {id} The id string of the specific entry
     *  return {Promise} Promise of the resolved Ajax Call result in JSON format
     */
    deleteItemById(itemId) {
        return this.specificItemById(itemId, 'DELETE');
    }


    /**
     *  Makes an ajax call for a specific entry
     *
     *  param:  {id} Id for the specific item
     *          {method} 'GET', 'DELETE', 'POST', 'PUT'
     *  return  {Promise} Promise for the resolved Ajax Call result in JSON format
     */
    specificItemById(itemId, method) {
        let promise = new Promise((resolve) => {
            try {
                fetch(this.apiUrl + itemId, { method: method })
                    .then(res => res.json())
                    .then(
                        result => {
                            resolve(result);
                        }
                    );
            } catch (err) {
                console.log(err);
            }
        });
        return promise;
    }

    /**
     *  Gets specific entries of the table.
     *
     *  param: {id} The string id list for the specific entries
     *  return {Promise} Promise of the resolved Ajax Call result in JSON format
     */
    getItemsById(itemIds) {
        let promise = new Promise((resolve) => {
            let fResult = [];
            (itemIds.forEach((itemId) => {
                this.getItemById(itemId).then(
                    result => {
                        fResult.push(result);
                    }
                );
            }));
            resolve(fResult);
        });
        return promise;
    }

    /**
     *  Deletes specific entries of the table.
     *
     *  param: {id} The string id list for the specific entries
     *  return {Promise} Promise of the resolved Ajax Call result in JSON format
     */
    deleteItemsById(itemIds) {
        let promise = new Promise((resolve) => {
            let fResult = [];
            (itemIds.forEach((itemId) => {
                this.deleteItemById(itemId).then(
                    result => {
                        fResult.push(result);
                    }
                );
            }));
            resolve(fResult);
        });
        return promise;
    }
}